<div class="extension-demo">
	Welcome to dcat-admin !
</div>

<style>
	.extension-demo {
		color: @primary;
	}
</style>

<script require="@celaraze.chemex-ldap-importer">
	$('.extension-demo').extensionDemo();
</script>
